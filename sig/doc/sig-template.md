
# openEuler Docs Special Interest Group（SIG）
English | [简体中文](./sig-template_cn.md)

The openEuler Docs SIG is used to build and continuously enrich the documentation of the openEuler project. It attracts interested developers to participate in the construction and release of the documents to jointly create a better documentation experience.


## SIG Goals

- Handle the build and release of openEuler's basic version documentation
- Participate in the discussion of the planning of the openEuler document, formulate the operation plan of the document, and respond to user feedback in a timely manner
- Check the document, find document problems and modify
- Multilingual processing of documents, involving multilingual translation and proofreading
- Help developers participate in openEuler's documentation contributions, provide standard and content editing support

## Leadership

### Maintainers
- Rudy_Tan

### Committers
- amy_Mayun
- fhxing


## Basic Information

### Project Introduction
    https: /gitee.com/openeuler/community/sig/doc/

### Mailing list
- doc@openeuler.org

### IRC Channel
- #openeuler-doc

### External Contact
- Rudy_Tan 

### Repositories 
- openeuler/docs/
