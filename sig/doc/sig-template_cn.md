# openEuler文档兴趣小组（SIG）
[English](./sig-template.md) | 简体中文

openuler文档兴趣小组用于构建并不断丰富openuler项目的文档。它吸引感兴趣的开发人员参与文档的构建和发布，共同创造更好的文档体验。


## SIG的目标

- 处理openEuler的基础的版本文档的构建和发布
- 参与讨论openEuler文档的规划，制定文档的运营规划，并及时响应用户反馈
- 检查文档，发现文档问题并修改
- 文档的多语言处理，涉及到多语言翻译和校对
- 帮助开发者参与openEuler的文档贡献，提供标准和内容编辑上的支持

## 管理成员

### Maintainers
- Rudy_Tan

### Committers
- amy_Mayun
- fhxing


## SIG基本信息

### 项目简介
    https: /gitee.com/openeuler/community/sig/doc/

### 邮件列表
- doc@openeuler.org

### IRC频道
- #openeuler-doc

### 对外联络人
- Rudy_Tan 

### 存储库
- openeuler/docs/

